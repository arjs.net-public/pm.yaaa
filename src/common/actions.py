""" actions module """
import os
from datetime import datetime
from . import log
from . import messages
from . import utils

# some constants
__author__ = 'alexrjs'


def check_options(args, config):
    """ check options """
    if not config:
        if args.debug:
            log.view('>', '[Debug] Empty config argument')

    checks = config.items('actions')
    if not checks:
        if args.debug:
            log.view('>', '[Debug] Empty check argument')

        return

    if not isinstance(checks, list):
        if args.debug:
            log.view('>', '[Debug] Not a tuple list')

        return

    for check, value in checks:
        _hidden = True if check.endswith('*') else False
        check = check[:-1] if check.endswith('*') else check
        if not hasattr(args, check):
            continue

        _section = 'option:{}'.format(check)
        _required = False
        if config.has_option(_section, 'required'):
            _required = config.getboolean(_section, 'required')

        _show_error = True
        if check in args and not _required:
            _show_error = False

        specific_checks = value.split(':')
        if len(specific_checks) > 1:
            specific_checks = specific_checks[0].split(',')
            if args.command not in specific_checks:
                continue

        do_checks = value.split(':')
        if len(do_checks) > 1:
            do_checks = do_checks[1].split(',')
        else:
            do_checks = do_checks[0].split(',')

        for do_check in do_checks:
            # see if the plugin has a 'register' attribute
            try:
                use_config = None
                use_section = None
                if 'extra_config' in do_check:
                    _tmp = do_check.split(':')
                    do_check = _tmp[0]
                    use_config = vars(args)[_tmp[1]]
                    use_section = _tmp[2]

                check_action = globals()[do_check]
            except AttributeError:
                # raise an exception, log a message,
                # or just ignore the problem
                pass
            else:
                # try to call it, without catching any errors
                if args.debug:
                    log.view('>', '[Debug] checking parameter with action: %s %s' % (check, do_check))

                variable = vars(args)[check]
                if variable:
                    if args.debug:
                        log.view('>', '[Debug] Used value for action: %s' % variable)

                    if check_action in [check_year]:
                        vars(args)[check] = check_action(variable, name=check)
                        if args.debug:
                            log.view('>', '[Debug] new value... {}'.format(vars(args)[check] if not _hidden else '*' * 5))
                    elif check_action in [path_create]:
                        if args.debug:
                            log.view('>', '[Debug] checking value... {}'.format(vars(args)[check] if not _hidden else '*' * 5))

                        check_action(variable, name=check, extra=args)
                    else:
                        if args.debug:
                            log.view('>', '[Debug] checking value... {}'.format(vars(args)[check] if not _hidden else '*' * 5))

                        check_action(variable, name=check, exception=_show_error)

                elif check_action in [extra_config]:
                    vars(args)[check] = check_action(variable, config=use_config, section=use_section, name=check)
                    if args.debug:
                        log.view('>', '[Debug] new value... {}'.format(vars(args)[check] if not _hidden else '*' * 5))

    return


def check_command(command, name=None, exception=True):
    """ check command """
    if not name:
        name = 'default'

    if not command:
        raise AttributeError('{} --{} {} ({})'.format(messages.MSG_E_0001, 'cmd', command, name))

    import commands
    if command not in commands.__all__:
        if exception:
            raise AttributeError('{} {} {}'.format(messages.MSG_E_0005, 'cmd', '|'.join(commands.__all__)))
        else:
            return False
    else:
        return True


def path_exists(pathname, name=None, exception=True):
    """ path exists """
    if not pathname:
        raise AttributeError('{} --{} {}'.format(messages.MSG_E_0001, name, 'pathname'))

    pathname = os.path.expanduser(pathname)
    if os.path.exists(pathname) and os.path.isdir(pathname):
        return True
    elif os.path.exists(pathname) and os.path.isfile(pathname):
        return path_exists(os.path.dirname(pathname), name=name, exception=exception)
    elif exception:
        raise IOError('{} --{} {}'.format(messages.MSG_E_0004, name, 'pathname'))
    else:
        return False


def path_create(pathname, name=None, extra=None, exception=True):
    """ path create """
    if pathname == '.':
        pn = os.getcwd()
    else:
        pn = pathname

    if not pathname.startswith('.') and not pathname.startswith(os.path.sep):
        if extra is not None and 'work' in extra:
            if extra.work == '.':
                pn = os.path.join(os.getcwd(), pathname)
            elif path_exists(extra.work, name=name, exception=exception):
                pn = os.path.join(extra.work, pathname)

    if not path_exists(pn, name=name, exception=False):
        os.makedirs(pn)

    return path_exists(pn, name=name)


def path_recreate(pathname, name=None, exception=True):
    """ path recreate """
    if path_exists(pathname, name=name, exception=False):
        try:
            os.removedirs(pathname)
        except OSError:
            import shutil
            shutil.rmtree(pathname)

    if os.path.exists(pathname):
        raise SystemError('{} --{} {}'.format(messages.MSG_E_0012, pathname, 'pathname'))

    return path_create(pathname, name=name, exception=exception)


def file_exists(filename, name=None, exception=True):
    """ file exists """
    if not filename:
        raise AttributeError('{} --{} {}'.format(messages.MSG_E_0001, name, 'filename'))

    if os.path.exists(filename) and os.path.isfile(filename):
        return True
    elif exception:
        raise IOError('{} --{} {} ({})'.format(messages.MSG_E_0002, name, 'filename', filename))
    else:
        return False


def file_create(filename, name=None, exception=True):
    """ file create """
    if not file_exists(filename, name=name, exception=False):
        with open(filename, 'wb') as _tf:
            _tf.write('')

    return file_exists(filename, name=name, exception=exception)


def file_recreate(filename, name=None, exception=True):
    """ file recreate """
    if file_exists(filename, name=name, exception=False):
        os.remove(filename)

    return file_create(filename, name=name, exception=exception)


def check_year(year, name=None, exception=True):
    """ check year """
    if not name:
        name = 'check_year'

    if not year:
        if exception:
            raise AttributeError('{} --{} {} ({})'.format(messages.MSG_E_0001, 'year', year, name))
        else:
            return 0

    if year == 'cy':
        return int(datetime.now().strftime('%Y'))
    elif year == 'ly':
        return int(datetime.now().strftime('%Y')) - 1
    elif year == 'ny':
        return int(datetime.now().strftime('%Y')) + 1
    elif isinstance(year, int):
        return int(year)


def extra_config(value, config=None, section=None, name=None):
    """ extra config """
    if not config or not section or not name:
        return value

    if not value:
        _configuration = utils.fetch_configuration(config)
        return _configuration.get(section, name) \
            if _configuration.has_option(section, name) else value
    else:
        return value
