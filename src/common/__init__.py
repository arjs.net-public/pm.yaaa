""" common module """
from sys import path
from os import curdir
from os.path import abspath, dirname
from .messages import *
from .shared import *
from .utils import *
from .actions import *
from .log import view, info, debug
path.append(dirname(dirname(abspath(__file__))))

# some constants
__author__ = 'alexrjs'
