"""
Command: analyse
"""
# imports
import os
import json
import time
import datetime
from collections import namedtuple
import common
from common.log import view, info, debug
from common.utils import get_content

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'commands'

# some variables
yml_files = {}
stati = {}
Info = namedtuple('Info', ['host', 'service', 'health_url', 'status_url', 'port'])


def version():
    """Simply returns the current version string"""
    return '{} - Version {}'.format(__name__.upper(), __version__)


def run(args):
    """Main run method; Handles the main load of the combination of parameters;
    :type args: object[]
    """
    view('>', 'Start')
    if args.debug:
        view('>', args)
        view('>', '{} {} {}'.format(__name__, __version__, __file__))

    view('>', 'Collect')
    if collect_files(args):
        view('>', 'Analyse')
        result = analyse_files(args)

        if result and args.store.lower() in ['new', 'update', 'insert']:
            view('>', 'Store')
            store(args)

        view('>', 'Done')
    else:
        view('>', 'Done, with problems')


def change_info_of_file(args, name=None, key=None, value=None):
    global yml_files
    current = yml_files
    parts = name.split(os.sep)
    for part in parts:
        if part.endswith('.yml'):
            _item = current.setdefault(part, {})
            if key in _item and not isinstance(_item[key], list):
                _tmp = _item[key]
                _item[key] = [_tmp]
                _item[key].append(value)
            elif key in _item and isinstance(_item[key], list):
                _item[key].append(value)
            else:
                _item[key] = value

            continue

        current.setdefault(part, {})
        entry = current.get(part, {})
        current = entry


def change_state_of_file(args, name, state):
    # change_info_of_file(args, name=name, key='state', value=state)
    global yml_files
    current = yml_files
    parts = name.split(os.sep)
    for part in parts:
        if part.endswith('.yml'):
            _item = current.setdefault(part, {})
            _item['state'] = state
            continue

        current.setdefault(part, {})
        entry = current.get(part, {})
        current = entry


def change_type_of_file(args, name, typ):
    # change_info_of_file(args, name=name, key='type', value=typ)
    global yml_files
    current = yml_files
    parts = name.split(os.sep)
    for part in parts:
        if part.endswith('.yml'):
            _item = current.setdefault(part, {})
            _item['type'] = typ
            continue

        current.setdefault(part, {})
        entry = current.get(part, {})
        current = entry


def collect_files(args):
    try:
        _input_dir_length = len(args.input)
        for filename in common.locate(['*.yml', '*.yaml'], root=args.input, recursive=True):
            shortname = filename[_input_dir_length:]
            if args.debug:
                common.view('.', 'Found: {file}'.format(file=filename))

            if args.verbose or args.debug:
                common.view('.', 'Shortend: {file}'.format(file=shortname))

            change_state_of_file(args, shortname, 'tbd')
            change_type_of_file(args, shortname, 'unknown')

        return True

    except ValueError as error:
        common.view('!', '{msg}'.format(msg=error))

    except Exception as error:
        common.view('!', '{msg}'.format(msg=error.msg))

    return False


def analyse_files(args):
    try:
        _input_dir_length = len(args.input)
        for filename in common.locate(['*.yml', '*.yaml'], root=args.input, recursive=True):
            shortname = filename[_input_dir_length:]
            yaml_data = common.read_yaml_data(filename)
            if not yaml_data:
                change_state_of_file(args, shortname, 'ignored')
                change_type_of_file(args, shortname, 'empty')
                common.debug(args.debug, 'Ignored (empty): {file}'.format(file=filename))
                continue

            if not isinstance(yaml_data, list):
                change_state_of_file(args, shortname, 'ignored')
                change_type_of_file(args, shortname, 'properties')
                _dir_name = os.path.basename(os.path.dirname(filename))
                if _dir_name.lower() in ['vars', 'defaults', 'group_vars', 'host_vars']:
                    common.debug(args.debug, 'Ignored (possible vars only): {file}'.format(file=filename))
                elif shortname.lower() in ['.gitlab-ci.yaml', '.gitlab-ci.yml']:
                    common.debug(args.debug, 'Ignored (possible gitlab pipeline): {file}'.format(file=filename))
                    change_type_of_file(args, shortname, 'pipeline')
                elif _dir_name.lower() in ['meta']:
                    if 'galaxy_info' in yaml_data:
                        common.view('.', 'Process: {file}'.format(file=shortname))
                        common.view('..', 'Name: {name} (ignored)'.format(name='GALAXY'))
                    else:
                        common.debug(args.debug, 'Ignored (possible meta only): {file}'.format(file=filename))
                else:
                    common.debug(args.debug, 'Ignored (no list): {file}'.format(file=filename))

                continue

            common.view('.', 'Process: {file}'.format(file=shortname))
            for item in yaml_data:
                if 'name' in item:
                    _name = item['name']
                    if 'hosts' in item and item['hosts']:
                        _hosts = item['hosts']
                        common.view('..', 'Play Name: {name}'.format(name=item['name']))
                        change_type_of_file(args, shortname, 'playbook')
                        change_info_of_file(args, name=shortname, key='playbook', value=_name)
                        common.view('..', 'Hosts: {hosts}'.format(hosts=_hosts))
                        change_info_of_file(args, name=shortname, key='hosts', value=_hosts)

                        if 'pre_tasks' in item and item['pre_tasks']:
                            common.view('..', 'Pre: {pre}'.format(pre='pre_tasks'))
                            for _pre in item['pre_tasks']:
                                if 'name' in _pre:
                                    change_info_of_file(args, name=shortname, key='pre_tasks', value=_pre['name'])
                                else:
                                    change_info_of_file(args, name=shortname, key='pre_tasks', value='pre_tasks')

                        if 'roles' in item and item['roles']:
                            _roles = item['roles']
                            common.view('..', 'Roles: {roles}'.format(roles=_roles))
                            for _role in _roles:
                                if isinstance(_role, dict):
                                    if 'when' in _role:
                                        change_info_of_file(args, name=shortname, key='roles', value="(when) %s" % _role['role'])
                                    else:
                                        change_info_of_file(args, name=shortname, key='roles', value=_role['role'])
                                else:
                                    change_info_of_file(args, name=shortname, key='roles', value=_role)

                        if 'tasks' in item and item['tasks']:
                            common.view('...', 'Tasks: {tasks}'.format(tasks=item['tasks']))
                            for _task in item['tasks']:
                                if 'name' in _task:
                                    change_info_of_file(args, name=shortname, key='tasks', value=_task['name'])
                                else:
                                    change_info_of_file(args, name=shortname, key='tasks', value='tasks')

                        if 'post_tasks' in item and item['post_tasks']:
                            common.view('..', 'Post: {post}'.format(post=item['post_tasks']))
                            for _post in item['post_tasks']:
                                if 'name' in _post:
                                    change_info_of_file(args, name=shortname, key='post_tasks', value=_post['name'])
                                else:
                                    change_info_of_file(args, name=shortname, key='post_tasks', value='post_tasks')

                    elif 'service' in item:
                        common.view('..', 'Service: {tasks}'.format(tasks=item['service']))
                        if 'name' in item:
                            change_info_of_file(args, name=shortname, key='task', value="(service) %s" % item['name'])
                        else:
                            change_info_of_file(args, name=shortname, key='task', value='(service) Task')

                    elif 'shell' in item:
                        common.view('..', 'Shell: {tasks}'.format(tasks=item['shell']))
                        if 'name' in item:
                            change_info_of_file(args, name=shortname, key='task', value="(shell) %s" % item['name'])
                        else:
                            change_info_of_file(args, name=shortname, key='task', value='(shell) Task')

                    elif 'scm' in item:
                        common.view('..', 'SCM: {tasks}'.format(tasks=item['scm']))
                        change_type_of_file(args, shortname, 'tasks')
                        if 'name' in item:
                            change_info_of_file(args, name=shortname, key='task', value="(scm) %s: %s %s" % (item['name'], item['scm'], item['src']))
                        else:
                            change_info_of_file(args, name=shortname, key='task', value='(scm) Task: %s %s' % (item['scm'], item['src']))

                    else:
                        common.view('..', 'Name: {name}'.format(name=_name))
                        change_type_of_file(args, shortname, 'tasks')
                        change_info_of_file(args, name=shortname, key='task', value=_name)

                    change_state_of_file(args, shortname, 'analysed')
                else:
                    for _entry in item:
                        common.debug(args.debug, 'Entry: {entry}'.format(entry=_entry))
                        if 'debug' in _entry:
                            _info = '{info}'.format(info=item['debug']).replace('msg=', '').replace('\"', '')
                            common.view('...', 'DEBUG: {entry}'.format(entry=_entry))
                            change_info_of_file(args, name=shortname, key='task', value=_info)
                            change_type_of_file(args, shortname, 'tasks')
                            change_state_of_file(args, shortname, 'analysed')

                        elif 'include' in _entry:
                            _info = item[_entry]
                            common.view('...', 'INCLUDE: {info}'.format(info=_info))
                            if 'when' in item:
                                change_info_of_file(args, name=shortname, key='task', value="when include %s" % _info)
                            else:
                                change_info_of_file(args, name=shortname, key='task', value="include %s" % _info)
                            change_type_of_file(args, shortname, 'tasks')
                            change_state_of_file(args, shortname, 'analysed')

                        elif 'roles' in _entry and item['roles']:
                            _roles = item['roles']
                            for _role in _roles:
                                if isinstance(_role, dict):
                                    common.debug(args.debug, 'ROLE: {entry}'.format(entry=_role))
                                    for _role_entry in _role:
                                        if _role_entry not in 'role':
                                            continue

                                        if isinstance(_role, dict):
                                            if 'when' in _role:
                                                change_info_of_file(args, name=shortname, key='roles', value="(when) %s" % _role['role'])
                                            else:
                                                change_info_of_file(args, name=shortname, key='roles', value=_role['role'])
                                        else:
                                            change_info_of_file(args, name=shortname, key='roles', value=_role)
                                else:
                                    common.debug(args.debug, 'ROLE: {value}'.format(value=_role))
                                    change_info_of_file(args, name=shortname, key='roles', value=_role)

                        elif 'service' in _entry:
                            _info = item[_entry]
                            common.view('...', 'SERVICE: {info}'.format(info=_info))
                            if 'name' in item:
                                change_info_of_file(args, name=shortname, key='task', value="(service) %s" % item['name'])
                            else:
                                change_info_of_file(args, name=shortname, key='task', value='(service) tasks %s' % _info)

                        elif 'shell' in _entry:
                            _info = item[_entry]
                            common.view('...', 'SERVICE: {info}'.format(info=_info))
                            if 'name' in item:
                                change_info_of_file(args, name=shortname, key='task', value="(shell) %s" % item['name'])
                            else:
                                change_info_of_file(args, name=shortname, key='task', value='(shell) tasks %s' % _info)

                        elif 'scm' in item:
                            common.view('..', 'SCM: {tasks}'.format(tasks=item['scm']))
                            change_type_of_file(args, shortname, 'tasks')
                            if 'name' in item:
                                change_info_of_file(args, name=shortname, key='task', value="(scm) %s: %s %s" % (item['name'], item['scm'], item['src']))
                            else:
                                change_info_of_file(args, name=shortname, key='task', value='(scm) Task: %s %s' % (item['scm'], item['src']))

                        elif _entry in ['hosts', 'become']:
                            change_type_of_file(args, shortname, 'playbook')
                            # change_state_of_file(args, shortname, 'analysed')

                        elif _entry in ['block']:
                            common.view('...', 'Block: {info}'.format(info='block'))
                            for _task in item[_entry]:
                                if isinstance(_task, dict):
                                    if 'name' in _task:
                                        change_info_of_file(args, name=shortname, key='task', value="(block) %s" % _task['name'])
                                    else:
                                        if 'debug' in _task:
                                            _info = '(block) {info}'.format(info=_task['debug']).replace('msg=', '').replace('\"', '')
                                        else:
                                            _info = 'Ansible Task'
                                        change_info_of_file(args, name=shortname, key='task', value=_info)
                                        # change_info_of_file(args, name=shortname, key='task', value='tasks')
                                else:
                                    common.view('...', 'Task: {value}'.format(value=_task))
                                    change_info_of_file(args, name=shortname, key='task', value=_task)
                            change_type_of_file(args, shortname, 'tasks')
                            # change_state_of_file(args, shortname, 'analysed')
                        else:
                            common.debug(args.debug, 'Skipped: {entry}'.format(entry=_entry))
                            # change_state_of_file(args, shortname, 'skipped')

                    change_state_of_file(args, shortname, 'analysed')

        return True

    except Exception as error:
        _msg = error # if error.message else error.problem if error.problem else '?'
        common.view('!', '{msg}'.format(msg=_msg))
        return False


def store(args):
    if args.store == 'new':
        filename = os.path.join(args.output, 'result.json')
        if os.path.exists(filename):
            os.remove(filename)

        common.write_json_data(filename, yml_files, sort=True)
    elif args.store == 'update':
        print("Update not implemented, yet")
    elif args.store == 'Insert':
        print("Insert not implemented, yet")
    else:
        common.view('?', 'Unknown mode: {mode}'.format(mode=args.mode))
