"""
Command: generate
"""
# imports
import os
import copy
import common
import common.messages
from common.log import view
from common.utils import get_content

# some constants
__author__ = 'alexrjs'
__version__ = '1.0.0.0'
__suite__ = 'commands'

# some variables
analyse_json = {}
configuration = None
these_types_only = []
generated_files = {}

# some predefines
before_me = {
    'AnsibleStartEvent': [],        
    'Setup': [],        
    'PreTasks': ['Setup'],
    'Roles': ['PreTasks', 'Setup'],
    'Tasks': ['Roles', 'PreTasks', 'Setup'],
    'PostTasks': ['Tasks', 'Roles', 'PreTasks', 'Setup'],        
    'AnsibleEndEvent': ['PostTasks', 'Tasks', 'Roles', 'PreTasks', 'Setup'],
}
follow_me = {
    'AnsibleStartEvent': ['Setup', 'PreTasks', 'Roles', 'Tasks', 'PostTasks'],        
    'Setup': ['PreTasks', 'Roles', 'Tasks', 'PostTasks'],        
    'PreTasks': ['Roles', 'Tasks', 'PostTasks', 'PostTasks'],
    'Roles': ['Tasks', 'PostTasks'],
    'Tasks': ['PostTasks'],
    'PostTasks': [],        
    'AnsibleEndEvent': [],        
}
position_me = {
    'File': [630, 0],
    'AnsibleStartEvent': [300, 100],
    'Setup': [300, 100],      
    'PreTasks': [700, 100],        
    'Roles': [900, 100],
    'Tasks': [1100, 100],
    'PostTasks': [1300, 100],        
    'AnsibleEndEvent': [1500, 100],        
}


def version():
    """Simply returns the current version string"""
    return '{} - Version {}'.format(__name__.upper(), __version__)


def run(args):
    """Main run method; Handles the main load of the combination of parameters;
    :type args: object[]
    """
    if args.debug:
        view('>', args)
        view('>', '{} {} {}'.format(__name__, __version__, __file__))

    view('>', 'Start')
    if args.debug:
        view('>', args)
        view('>', '{} {} {}'.format(__name__, __version__, __file__))

    view('>', 'Read Config')
    global these_types_only
    global configuration    
    _config_file = os.path.realpath(os.path.join(os.path.dirname(__file__), 'generate.config'))
    if _config_file and os.path.exists(_config_file):
        common.configuration = common.fetch_configuration(_config_file)
    else:
        _config_file = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))
        _config_file = os.path.join(_config_file, 'generate.config')
        if _config_file and os.path.exists(_config_file):
                common.configuration = common.fetch_configuration(_config_file)
        else:
            raise EnvironmentError('{} ({})'.format(common.messages.MSG_E_0002, _config_file))

    configuration = common.fetch_configuration(_config_file)
    # print configuration.items('general')
    for key, value in configuration.items('general'):
        if key in 'these_types_only':
            these_types_only = value.split(',')

    view('>', 'Collect')    
    if collect(args): 
        view('>', 'Generate')
        result = generate(args)

        if result and args.store.lower() in ['new', 'update', 'insert']:
            view('>', 'Store')
            store(args)
            store_result(args)

        view('>', 'Done')
    else:
        view('!', 'Problems reading the analyse file.')


def collect(args):
    try:
        global analyse_json
        analyse_json = common.read_json_data(os.path.realpath(args.input))
        return True

    except OSError as error:
        common.view('!', '{msg}'.format(msg=error))
        return False

    except Exception as error:
        _msg = error if 'msg' not in error else error.msg
        common.view('!', '{msg}'.format(msg=_msg))
        return False


def generate(args, node=None, path=None, indent=0):
    global generated_files
    try:
        # common.pprint(analyse_json)
        if not node:
            _current_node = analyse_json
            _current_path = path if path else args.output
        else:
            _current_node = node
            _current_path = path

        for _item in _current_node:
            if _item.endswith('.yml') or _item.endswith('.yaml'):
                _type = _current_node[_item]['type']
                if _type in these_types_only:
                    _file_name = os.path.join(_current_path, _item)
                    generated_files.setdefault(_file_name, generate_bpmn_json(args, _current_node[_item]))
                    change_state_of_file(args, _file_name, 'generated')
                    common.view('.' * (indent + 2), 'Generated File %s of type %s' % (_file_name, _type))
                else:
                    common.view('.' * (indent + 2), 'Ignored File %s of type %s' % (_item, _type))

                continue
            else:
                common.view('.' * (indent + 2), _item)
                 
            if isinstance(_current_node[_item], dict):                
                generate(args, node=_current_node[_item], path=os.path.join(_current_path, _item), indent=indent + 1)

        return True

    except Exception as error:
        common.view('!', '{msg}'.format(msg=error))
        return False


def store(args):
    for _file in generated_files:
        # print _file
        # print generated_files[_file]
    
        if args.store == 'new':
            if not os.path.exists(os.path.dirname(_file)):
                os.makedirs(os.path.dirname(_file))
            
            _filename = _file.replace('.yml', '.json')
            if os.path.exists(_filename):
                os.remove(_filename)

            common.write_json_data(_filename, generated_files[_file], sort=False)

            _filename = _file.replace('.yml', '.bpmn')
            if os.path.exists(_filename):
                os.remove(_filename)

            _short_name = _file.replace(args.output, '')
            common.view('.', 'Writing bpmn file %s' % _filename)
            write_bpmn_data(args, _filename, generate_bpmn_data(args, generated_files[_file], _short_name))
        elif args.store == 'update':
            print("Update not implemented, yet")
        elif args.store == 'Insert':
            print("Insert not implemented, yet")
        else:
            common.view('?', 'Unknown mode: {mode}'.format(mode=args.mode))

    return True


def store_result(args):
    if args.store == 'new':
        filename = os.path.join(args.output, 'result.json')
        if os.path.exists(filename):
            os.remove(filename)

        common.write_json_data(filename, analyse_json, sort=True)
    elif args.store == 'update':
        print("Update not implemented, yet")
    elif args.store == 'Insert':
        print("Insert not implemented, yet")
    else:
        common.view('?', 'Unknown mode: {mode}'.format(mode=args.mode))


def change_state_of_file(args, name, state):
    # change_info_of_file(args, name=name, key='state', value=state)
    global analyse_json
    _tmp_name = name.replace(args.output, '')[1:]
    current = analyse_json
    parts = _tmp_name.split(os.sep)
    for part in parts:
        if part.endswith('.yml'):
            _item = current.setdefault(part, {})
            _item['state'] = state
            continue

        current.setdefault(part, {})
        entry = current.get(part, {})
        current = entry


def generate_bpmn_json(args, node=None):
    _definitions = {
        "?xml": {
            "attributes": {
                "version": "1.0",
                "encoding": "UTF-8"
            }
        },
        "bpmn:definitions": {
            "attributes": {
                "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
                "xmlns:bpmn": "http://www.omg.org/spec/BPMN/20100524/MODEL",
                "xmlns:bpmndi": "http://www.omg.org/spec/BPMN/20100524/DI",
                "xmlns:dc": "http://www.omg.org/spec/DD/20100524/DC",
                "xmlns:di": "http://www.omg.org/spec/DD/20100524/DI",
                "id": "Ansible_Process_Definitions",
                "targetNamespace": "http://bpmn.io/schema/bpmn",
            },
            "data": {
                "bpmn:process": {
                    "attributes": {
                        "id": "Ansible_Process",
                        "isExecutable": "false",
                    },
                    "data": {
                        # "Setup": {},
                        # "PreTasks": [],
                        # "RolesTasks": [],
                        # "PostTasks": [],
                    }
                },
                "bpmndi:BPMNDiagram": {
                    "attributes": {
                        "id": "Ansible_Diagram"
                    },
                    "data": {
                        "bpmndi:BPMNPlane": {
                            "attributes": {
                                "id": "Ansible_Diagram_Plane",
                                "bpmnElement": "Ansible_Process"
                            },
                            "data": {}
                        }
                    }
                }
            }
        }
    }
    _current_definitions = {}
    if node:
        _current_definitions = copy.deepcopy(_definitions)
        _current_data = _current_definitions["bpmn:definitions"]["data"]["bpmn:process"]["data"]
        for _item in node:
            if _item in 'playbook':
                _current_data['Setup'] = {'name': 'Setup: %s' % node[_item]}
            elif _item == 'pre_tasks':
                _tmp = _current_data.setdefault('PreTasks', []) 
                if isinstance(node[_item], list):
                    for _entry in node[_item]:
                        _tmp.append('Pre-Task: %s' % _entry)
                else:
                    _tmp.append('Pre-Task: %s' % node[_item])
            elif _item == 'roles':
                _tmp = _current_data.setdefault('Roles', []) 
                if isinstance(node[_item], list):
                    for _entry in node[_item]:
                        _tmp.append('Role: %s' % _entry)
                else:
                    _tmp.append('Role: %s' % node[_item])
            elif _item == 'tasks' or _item == 'task':
                _tmp = _current_data.setdefault('Tasks', []) 
                if isinstance(node[_item], list):
                    for _entry in node[_item]:
                        _tmp.append('Task: %s' % _entry)
                else:
                    _tmp.append('Task: %s' % node[_item])
            elif _item == 'post_tasks':
                _tmp = _current_data.setdefault('PostTasks', []) 
                if isinstance(node[_item], list):
                    for _entry in node[_item]:
                        _tmp.append('Post-Task: %s' % _entry)
                else:
                    _tmp.append('Post-Task: %s' % node[_item])
            else:
                if node['type'] == 'playbook' and 'Setup' not in _current_data:
                    _current_data['Setup'] = {'name': 'Setup: %s' % node[_item]}
    
                pass

        # not needed for json
        # _current_data = _current_definitions["bpmn:definitions"]["data"]["bpmn:BPMNDiagram"]["data"]

    return _current_definitions


def build_subprocess(args, data=None, flows=None, process=None, element=None, file_link=False):
    if data is None or flows is None or process is None or element is None:
        return None

    import xml.etree.ElementTree as ET

    _sequence = 'Sequence_%s' % element
    _tmp_task = ET.SubElement(process, "bpmn:subProcess", {"id": 'Task_%s' % element, 'name': element})
    _tmp_in = ET.SubElement(_tmp_task, "bpmn:incoming")
    _tmp_out = ET.SubElement(_tmp_task, "bpmn:outgoing")
    flows.setdefault(_sequence, {})
    _current_flow = flows[_sequence] 

    for _item in before_me[element]:
        if _item in data['data']:
            _in = 'Task_%s' % _item
            break
    else:
        _in = 'Sequence_AnsibleStartEvent'

    for _item in follow_me[element]:
        if _item in data['data']:
            _target = 'Task_%s' % _item
            _out = 'Sequence_%s' % _item
            break
    else:
        _target = 'AnsibleEndEvent'
        _out = 'Sequence_AnsibleEndEvent'

    _tmp_in.text = _in
    _tmp_out.text = _out
    _current_flow['sourceRef'] = 'Task_%s' % element
    _current_flow['targetRef'] = _target

    if element == "Setup" or file_link:
        #   <bpmn:property id="Property_0anufz2" name="__targetRef_placeholder" />
        _tmp = ET.SubElement(_tmp_task, "bpmn:property", { "id": "PropertyFile", "name": "__targetRef_placeholder" })
        #   <bpmn:dataInputAssociation id="DataInputAssociation_179f0nc">
        _tmp = ET.SubElement(_tmp_task, "bpmn:dataInputAssociation", { "id": "DI_File_Setup" })
        #     <bpmn:sourceRef>DataObject_File</bpmn:sourceRef>
        ET.SubElement(_tmp, "bpmn:sourceRef").text = "DataObject_File"
        #     <bpmn:targetRef>Property_0anufz2</bpmn:targetRef>
        ET.SubElement(_tmp, "bpmn:targetRef").text = "PropertyFile"
        #   </bpmn:dataInputAssociation>

    return _tmp_task


def generate_bpmn_processes(args, data, root, filename):
    import random
    import xml.etree.ElementTree as ET
    
    _flows = {}
    if 'attributes' in data:
        _attributes = data['attributes']
        _process = ET.SubElement(root, 'bpmn:process', _attributes)
    if 'data' in data:
        if not data['data']:
            return _flows, {}

        # Generate Start Event
        _tmp_start = ET.SubElement(_process, "bpmn:startEvent", {"id": "AnsibleStartEvent"})
        _tmp = ET.SubElement(_tmp_start, "bpmn:outgoing")
        _tmp.text = "Sequence_AnsibleStartEvent"
        ET.SubElement(_tmp_start, "bpmn:signalEventDefinition")
        for _item in follow_me['AnsibleStartEvent']:
            if _item in data['data']:
                _target = 'Task_%s' % _item
                break
        else:
            _target = 'AnsibleEndEvent'

        _flows.setdefault('Sequence_AnsibleStartEvent', {'sourceRef': 'AnsibleStartEvent', 'targetRef': _target})

        # Generate Step Events
        _hierarchy = {}
        for _element in data["data"]:
            if _element in ['Setup', 'PreTasks', 'Roles', 'Tasks', 'PostTasks']:
                _add_file_link = ('Setup' not in data["data"])
                _subtask = build_subprocess(args, data=data, flows=_flows, process=_process, element=_element, file_link=_add_file_link)
                if len(data["data"][_element]) and _subtask is not None:
                    if 'name' in data["data"][_element]:
                        continue

                    _previous_task = _subtask
                    _hierarchy.setdefault(_subtask.attrib['id'], [])
                    for _item in data["data"][_element]:
                        # common.view('-', 'Item: %s' % _item)
                        _item = _item.replace('|', '\n')
                        if 'DEBUG' in _item.upper():
                            continue

                        _thash = "%032x" % random.getrandbits(128)
                        _target = 'Task_%s' % _thash
                        _hierarchy.setdefault(_subtask.attrib['id'], []).append(_target)

                        _shash = "%032x" % random.getrandbits(128)
                        _sequence = 'Sequence_%s_%s' % (_element, _shash)

                        _flows.setdefault(_sequence, {})
                        _current_flow = _flows[_sequence] 
                        _current_flow['sourceRef'] = _previous_task.attrib['id']
                        _current_flow['targetRef'] = _target

                        _tmp_out = ET.SubElement(_previous_task, "bpmn:outgoing")
                        _tmp_out.text = _sequence

                        if 'include' in _item:
                            if 'when' in _item:
                                _tmp_task = ET.SubElement(_process, "bpmn:businessRuleTask", {"id": _target, 'name': _item})
                            else:
                                _tmp_task = ET.SubElement(_process, "bpmn:Task", {"id": _target, 'name': _item})
                        elif '(service)' in _item:
                            _tmp_task = ET.SubElement(_process, "bpmn:serviceTask", {"id": _target, 'name': _item})
                        elif '(shell)' in _item:
                            _tmp_task = ET.SubElement(_process, "bpmn:scriptTask", {"id": _target, 'name': _item})
                        elif '(block)' in _item:
                            _tmp_task = ET.SubElement(_process, "bpmn:businessRuleTask", {"id": _target, 'name': _item})
                        elif '(when)' in _item:
                            _tmp_task = ET.SubElement(_process, "bpmn:businessRuleTask", {"id": _target, 'name': _item})
                        else:
                            _tmp_task = ET.SubElement(_process, "bpmn:Task", {"id": _target, 'name': _item})

                        _tmp_in = ET.SubElement(_tmp_task, "bpmn:incoming")
                        _tmp_in.text = _sequence

                        _previous_task = _tmp_task

            else:
                common.view('#', 'Ignored element %s' % _element)

        # Generate End Event
        _tmp_end = ET.SubElement(_process, "bpmn:endEvent", {"id": "AnsibleEndEvent"})
        _tmp = ET.SubElement(_tmp_end, "bpmn:incoming")
        _tmp.text = "AnsibleStartEvent"
        ET.SubElement(_tmp_end, "bpmn:signalEventDefinition")
        for _item in before_me['AnsibleEndEvent']:
            if _item in data['data']:
                _source = 'Task_%s' % _item
                break
        else:
            _source = 'AnsibleStartEvent'

        _flows.setdefault('Sequence_AnsibleEndEvent', {'sourceRef': _source, 'targetRef': 'AnsibleEndEvent'})
    else:
        pass

    # Generate Flows
    for _flow in _flows:
        if 'sourceRef' in _flows[_flow] and 'targetRef' in _flows[_flow]:
            ET.SubElement(_process, 'bpmn:sequenceFlow', {'id': _flow, 'sourceRef': _flows[_flow]['sourceRef'], 'targetRef': _flows[_flow]['targetRef']})
        else:
            pass
            # view('!', 'Ignored Flow! (%s)' % _flow)                

    # Generate Annotations
    _tmp_text = ET.SubElement(_process, 'bpmn:textAnnotation', {'id': 'TextAnnotation_Playbook'})
    _tmp = ET.SubElement(_tmp_text, 'bpmn:text')
    _tmp.text = "File: %s" % (filename)
    if 'Setup' in data['data']:
        if 'name' in data['data']['Setup']:
            # _tmp.text = "Playbook: %s\nFile: %s" % (data['data']['Setup']['name'].replace('Setup: ', ''), filename)
            _tmp.text = "Playbook: %s" % (data['data']['Setup']['name'].replace('Setup: ', '').replace('|', '\n'))

    ET.SubElement(_process, 'bpmn:sequenceFlow', {'id': 'Association_Playbook', 'sourceRef': 'DataObject_File', 'targetRef': 'TextAnnotation_Playbook'})
    # <bpmn:dataObjectReference id="DataObjectReference_0io14ik" name="/playbooks/apps/pyapp.yml" dataObjectRef="DataObject_0tcoqhd" />
    ET.SubElement(_process, 'bpmn:dataObjectReference', {'id': 'DataObject_File', 'name': filename, 'dataObjectRef': 'DataObject_File_Details'})
    # <bpmn:dataObject id="DataObject_0tcoqhd" />
    ET.SubElement(_process, 'bpmn:dataObject', {'id': 'DataObject_File_Details'})
    # <bpmn:association id="Association_0v3ugpt" sourceRef="DataObjectReference_0io14ik" targetRef="TextAnnotation_Playbook" />
    ET.SubElement(_process, 'bpmn:association', {'id': 'Association_File_Playbook', 'sourceRef': 'DataObject_File', 'targetRef': 'TextAnnotation_Playbook'})

    return _flows, _hierarchy


def generate_bpmn_standard_shapes(args, plane=None, flows=None):
    import xml.etree.ElementTree as ET

    _shapes = {}

    # Generate start shape
    _tmp_start = ET.SubElement(plane, "bpmndi:BPMNShape", {"id": "ShapeAnsibleStartEvent", "bpmnElement": "AnsibleStartEvent"})
    _shapes.setdefault("AnsibleStartEvent", _tmp_start)
    _x = position_me['AnsibleStartEvent'][0]
    _y = position_me['AnsibleStartEvent'][1] + 22
    # print "%s" % _x, "%s" % _y
    ET.SubElement(_tmp_start, "dc:Bounds", {"x": "%s" % _x, "y": "%s" % _y, "width": "36", "height": "36" })
    _tmp = ET.SubElement(_tmp_start, "bpmndi:BPMNLabel")
    ET.SubElement(_tmp, "dc:Bounds", {"x": "0", "y": "%s" % _y, "width": "90", "height": "20"})
    
    # Generate step shapes
    _c = 0
    for _item in follow_me['AnsibleStartEvent']:
        _flow = 'Sequence_%s' % _item
        if _flow not in flows:
            continue
            
        # print _item, _flow
        if 'sourceRef' in flows[_flow]: 
            _source = flows[_flow]['sourceRef']
            if _source in 'ShapeAnsibleStartEvent':
                continue
        else:
            continue

        if _source not in _shapes:
            _c += 1
            _x = 100 + _c * 400
            _y = 100
            # print "%s" % _x, "%s" % _y
            _tmp_task = ET.SubElement(plane, "bpmndi:BPMNShape", {"id": "Shape%s" % _source, "bpmnElement": _source})
            _shapes.setdefault(_source, _tmp_task)
            ET.SubElement(_tmp_task, "dc:Bounds", {"x": "%s" % _x, "y": "%s" % _y, "width": "300", "height": "80" })

    # Generate end shape
    _tmp_end = ET.SubElement(plane, "bpmndi:BPMNShape", {"id": "ShapeAnsibleEndEvent", "bpmnElement": "AnsibleEndEvent"})
    _shapes.setdefault("AnsibleEndEvent", _tmp_end)
    _c += 1
    _x = 100 + _c * 400
    _y = 100 + 22
    # print "%s" % _x, "%s" % _y
    ET.SubElement(_tmp_end, "dc:Bounds", {"x": "%s" % _x, "y": "%s" % _y, "width": "36", "height": "36" })
    _tmp = ET.SubElement(_tmp_end, "bpmndi:BPMNLabel")
    ET.SubElement(_tmp, "dc:Bounds", {"x": "%s" % (_x + 27), "y": "136", "width": "90", "height": "20"})

    return _shapes


def generate_bpmn_task_shapes(args, plane=None, flows=None, shapes=None, hierarchy=None):
    import xml.etree.ElementTree as ET

    for _flow in flows:
        # common.view('-', 'Flow: %s' % _flow)
        if 'sourceRef' in flows[_flow]: 
            _source = flows[_flow]['sourceRef']
        else:
            continue

        if _source not in shapes or _source not in hierarchy:
            continue

        # print "Handle: %s" % _source
        _c = 0
        _bounds = shapes[_source][0]
        for _item in hierarchy[_source]:
            if _item not in shapes:
                _c += 1
                _x = int(_bounds.attrib['x'])
                _y = int(_bounds.attrib['y']) + _c * 125
                # print "%s" % _x, "%s" % _y
                _tmp_task = ET.SubElement(plane, "bpmndi:BPMNShape", {"id": "Shape%s" % _item, "bpmnElement": _item})
                shapes.setdefault(_item, _tmp_task)
                ET.SubElement(_tmp_task, "dc:Bounds", {"x": "%s" % _x, "y": "%s" % _y, "width": "300", "height": "80" })


def generate_bpmn_standard_edges(args, plane=None, flows=None):
    import xml.etree.ElementTree as ET

    _edges = {}
    _c = 0
    _x = position_me['AnsibleStartEvent'][0] + _c * 400
    _y = position_me['AnsibleStartEvent'][1]
    # print "%s" % _x, "%s" % _y
    _tmp_edge = ET.SubElement(plane, "bpmndi:BPMNEdge", {"id": "EdgeAnsibleStartEvent", "bpmnElement": 'Sequence_AnsibleStartEvent'})
    _edges.setdefault('AnsibleStartEvent', _tmp_edge)
    _add_x1 = 36
    _add_x2 = 400
    ET.SubElement(_tmp_edge, "di:waypoint", {"xsi:type": "dc:Point", "x": "%s" % (_x + _add_x1), "y": "%s" % (_y + 40)})
    ET.SubElement(_tmp_edge, "di:waypoint", {"xsi:type": "dc:Point", "x": "%s" % (_x + _add_x2), "y": "%s" % (_y + 40)})
    _tmp = ET.SubElement(_tmp_edge, "bpmndi:BPMNLabel")
    ET.SubElement(_tmp, "dc:Bounds", {"x": "%s" % (_x + 36), "y": "120", "width": "0", "height": "12"})

    #   <bpmndi:BPMNShape id="DataObjectReference_0io14ik_di" bpmnElement="DataObjectReference_0io14ik">
    _tmp_file = ET.SubElement(plane, "bpmndi:BPMNShape", {"id": "ShapeDataObject_File", "bpmnElement": "DataObject_File"})
    #     <dc:Bounds x="179" y="45" width="36" height="50" />
    _x = position_me['File'][0]
    _y = position_me['File'][1]
    ET.SubElement(_tmp_file, "dc:Bounds", {"x": "%s" % _x, "y": "%s" % _y, "width": "36", "height": "50" })
    #     <bpmndi:BPMNLabel>
    _tmp = ET.SubElement(_tmp_file, "bpmndi:BPMNLabel")
    #       <dc:Bounds x="156" y="102" width="83" height="27" />
    ET.SubElement(_tmp, "dc:Bounds", {"x": "%s" % (_x - 25), "y": "%s" % (_y - 30), "width": "84", "height": "27"})
    #     </bpmndi:BPMNLabel>
    #   </bpmndi:BPMNShape>
    #   <bpmndi:BPMNEdge id="Association_0v3ugpt_di" bpmnElement="Association_0v3ugpt">
    _tmp_edge = ET.SubElement(plane, "bpmndi:BPMNEdge", {"id": "Edge%s" % "Association_File", "bpmnElement": "Association_File_Playbook"})
    _x = position_me['File'][0]
    _y = position_me['File'][1] + 25
    #     <di:waypoint x="215" y="70" />
    ET.SubElement(_tmp_edge, "di:waypoint", {"xsi:type": "dc:Point", "x": "%s" % (_x + 35), "y": "%s" % _y})
    #     <di:waypoint x="292" y="70" />
    ET.SubElement(_tmp_edge, "di:waypoint", {"xsi:type": "dc:Point", "x": "%s" % (_x + 170), "y": "%s" % _y})
    #   </bpmndi:BPMNEdge>

    # Generate Text Annotation Shape
    _tmp_end = ET.SubElement(plane, "bpmndi:BPMNShape", {"id": "ShapeTextAnnotation", "bpmnElement": "TextAnnotation_Playbook"})
    _x = position_me['File'][0] + 170
    _y = position_me['File'][1] + 10
    # print "%s" % _x, "%s" % _y
    ET.SubElement(_tmp_end, "dc:Bounds", {"x": "%s" % _x, "y": "%s" % _y, "width": "500", "height": "40" })
    
    # Generate Text Annotation Edge
    _tmp_edge = ET.SubElement(plane, "bpmndi:BPMNEdge", {"id": "EdgeDI_File_Setup", "bpmnElement": 'DI_File_Setup'})
    _x = position_me['File'][0] + 18
    _y = position_me['File'][1]
    # print("%s" % _x, "%s" % _y)
    ET.SubElement(_tmp_edge, "di:waypoint", {"xsi:type": "dc:Point", "x": "%s" % _x, "y": "%s" % (_y + 50)})
    ET.SubElement(_tmp_edge, "di:waypoint", {"xsi:type": "dc:Point", "x": "%s" % _x, "y": "%s" % (_y + 100)})

    for _item in follow_me['AnsibleStartEvent']:
        _flow = 'Sequence_%s' % _item
        if _flow not in flows:
            continue
            
        # print _item, _flow
        if 'AnsibleEndEvent' in _flow:
            continue

        if _flow not in _edges:
            _c += 1
            _x = 100 + _c * 400
            _y = 100
            # print "%s" % _x, "%s" % _y
            _tmp_edge = ET.SubElement(plane, "bpmndi:BPMNEdge", {"id": "Edge%s" % _flow, "bpmnElement": _flow})
            _edges.setdefault(_flow, _tmp_edge)
            if _flow.endswith('Event'):
                _add_x1 = 36
                _add_x2 = 150
            else:
                _add_x1 = 100
                _add_x2 = 400
            
            ET.SubElement(_tmp_edge, "di:waypoint", {"xsi:type": "dc:Point", "x": "%s" % (_x + _add_x1), "y": "%s" % (_y + 40)})
            ET.SubElement(_tmp_edge, "di:waypoint", {"xsi:type": "dc:Point", "x": "%s" % (_x + _add_x2), "y": "%s" % (_y + 40)})

            _tmp = ET.SubElement(_tmp_edge, "bpmndi:BPMNLabel")
            ET.SubElement(_tmp, "dc:Bounds", {"x": "%s" % (_x + 36), "y": "120", "width": "0", "height": "12"})

    return _edges


def generate_bpmn_task_edges(args, plane=None, flows=None, shapes=None, edges=None, hierarchy=None):
    import xml.etree.ElementTree as ET

    _c = -1
    for _flow in flows:
        # common.view('-', 'Flow: %s' % _flow)
        if 'sourceRef' in flows[_flow]: 
            _source = flows[_flow]['sourceRef']
        else:
            continue

        if _flow in edges:
            continue

        if 'AnsibleStartEvent' in _flow:
            continue

        if 'AnsibleEndEvent' in _flow:
            continue

        # print "Handle: %s" % _source
        _bounds = shapes[_source][0]
        _x = int(_bounds.attrib['x']) + 150
        _y = int(_bounds.attrib['y']) + 80
        # print "%s" % _x, "%s" % _y
        _tmp_edge = ET.SubElement(plane, "bpmndi:BPMNEdge", {"id": "Edge%s" % _flow, "bpmnElement": _flow})
        edges.setdefault(_flow, _tmp_edge)
        
        ET.SubElement(_tmp_edge, "di:waypoint", {"xsi:type": "dc:Point", "x": "%s" % _x, "y": "%s" % (_y)})
        ET.SubElement(_tmp_edge, "di:waypoint", {"xsi:type": "dc:Point", "x": "%s" % _x, "y": "%s" % (_y + 45)})

        _tmp = ET.SubElement(_tmp_edge, "bpmndi:BPMNLabel")
        ET.SubElement(_tmp, "dc:Bounds", {"x": "%s" % (_x + 36), "y": "120", "width": "0", "height": "12"})


def generate_bpmn_diagrams(args, data, root, flows, hierarchy):
    import xml.etree.ElementTree as ET

    if 'attributes' in data:
        _attributes = data['attributes']
        _diagrams = ET.SubElement(root, 'bpmndi:BPMNDiagram', _attributes)
    
    if 'data' in data:
        _data = data['data']["bpmndi:BPMNPlane"]
        # Generate plane        
        _attributes = _data['attributes']
        _tmp_plane = ET.SubElement(_diagrams, "bpmndi:BPMNPlane", _attributes)

        # Generate Standard Shapes
        _shapes = generate_bpmn_standard_shapes(args, plane=_tmp_plane, flows=flows)

        # Generate Task Shapes
        generate_bpmn_task_shapes(args, plane=_tmp_plane, flows=flows, shapes=_shapes, hierarchy=hierarchy)

        # Generate Standard Edges
        _edges = generate_bpmn_standard_edges(args, plane=_tmp_plane, flows=flows)

        # Generate Task Edges
        generate_bpmn_task_edges(args, plane=_tmp_plane, flows=flows, shapes=_shapes, edges=_edges, hierarchy=hierarchy)


def generate_bpmn_data(args, data, filename):
    import xml.etree.ElementTree as ET
    from xml.dom import minidom

    _root = None
    _process = None
    xml_attribs = {}
    for _entry in data:
        # print _entry
        if _entry.startswith('?'):
            xml_attribs = data[_entry]['attributes']
        elif _entry.startswith('bpmn:definitions'):
            _root = ET.Element(_entry, data[_entry]['attributes'])
            _flows, _hierarchy = generate_bpmn_processes(args, data[_entry]["data"]['bpmn:process'], _root, filename)
            generate_bpmn_diagrams(args, data[_entry]["data"]['bpmndi:BPMNDiagram'], _root, _flows, _hierarchy)

    return minidom.parseString(ET.tostring(_root, encoding=xml_attribs['encoding'], method='xml')).toprettyxml(indent="  ")


def write_bpmn_data(args, filename, data, debug=False):
    if not filename:
        raise AttributeError('{} ({})'.format(common.messages.MSG_E_0001, filename))

    if debug:
        view('>', '[Debug] Filename: {}'.format(filename))
        view('>', '[Debug] Data: {}'.format(data))

    with open(filename, 'w') as text_file:
        text_file.write(data)
