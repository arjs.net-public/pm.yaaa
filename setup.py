#!/usr/bin/env python
import os
from setuptools import setup, find_packages


def read(filename):
    """
    Just read a text file with information
    """
    return open(os.path.join(os.path.dirname(__file__), filename)).read()

setup(
    name=read('config/name.txt'),
    version=read('config/version.txt'),
    author='Alexander Schiessl',
    author_email='alexrjs@gmail.com',
    url='http://arjs.net',
    license = "Unlicense.org",    
    packages=find_packages('src'), 
    package_dir={'':'src'}, 
    package_data={
        '': ['../config/*.txt', '*.config'],
        '': ['*.config'],
    },
    include_package_data=True,
    data_files=[('bin/yaaa.config', ['src/default.config', 'src/generate.config', 'LICENSE', 'README.md'])],
    install_requires=['PyYAML', 'docopt'],
    description=read('config/short.txt'),
    long_description=read('config/description.txt'),
    scripts=['src/bin/yaaa'],
    platforms=['MacOS', 'Linux'],
    zip_safe=False,
    classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: Console',
          'Intended Audience :: Developers',
          'Intended Audience :: System Administrators',
          'License :: UnLicense.org',
          'Operating System :: MacOS :: MacOS X',
          'Operating System :: Linux',
          'Programming Language :: Python',
          'Topic :: Communications :: Email',
          'Topic :: Office/Business',
          'Topic :: Software Development :: Bug Tracking',
          ],
)
